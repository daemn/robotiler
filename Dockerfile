FROM nginx:1.21 as app

WORKDIR /usr/share/nginx/html

COPY favicon.png ./
COPY unicode.txt ./
COPY nginx_robotiler.conf /etc/nginx/conf.d/
COPY *.js ./
COPY *.html ./
COPY *.css ./
COPY *.py ./
