__doc__ = """
Общая библиотека.
"""


class Log:
    """
    Вывод сообщений в консоль.
    Соответствие уровней лога и уровней сообщений:

                      Уровень сообщения
                     debug   info   error
    Уровень  quiet |  -    |  -   |   -  |
       лога  error |  -    |  -   |   V  |
             info  |  -    |  V   |   V  |
             debug |  V    |  V   |   V  |
    """
    level: str

    def __init__(self, level: str) -> None:
        """
        :param level: Уровень логирования ('debug', 'info', 'error', 'quiet').
        """
        self.level = level

    def debug(self, msg: str) -> None:
        """
        Выводит сообщение, если уровень debug.

        :param msg: Сообщение.
        """
        if self.level == 'debug':
            print('[DEBUG]', msg)

    def info(self, msg: str) -> None:
        """
        Выводит сообщение, если уровень info или debug.

        :param msg: Сообщение.
        """
        if self.level in ('info', 'debug'):
            print('[INFO]', msg)

    def error(self, msg: str) -> None:
        """
        Выводит сообщение если уровень error или больше.

        :param msg: Сообщение.
        """
        if self.level in ('error', 'info', 'debug'):
            print('[ERROR]', msg)
