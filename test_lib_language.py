# Так как модуль pytest и другие тестовые в браузере не импортируются, а модуль
# browser не импортируется вне браузера, пришлось закостылить такую систему.
# Чтобы сохранить порядок тестовых функций, приходится добавлять в имя
# номер для сортировки.

from browser import document, html
import lib_language
lib_language.log.level = 'quiet'


def convert_to_slices(data: list) -> list:
    """
    Из дерева со строками, делает дерево со слайсами.

    :param data: Одно или многоуровневый список со строками.
    :return: Одно или многоуровневый список со слайсами.
    """
    result: list = []
    for note in data:
        if isinstance(note, str) or isinstance(note, int):
            result.append(lib_language.CodeSlice(1, 2, note))
        else:
            result.append(convert_to_slices(note))
    return result


class Test1CodeSlice:
    """Проверяет тип данных CodeSlice"""

    def test_1(self) -> None:
        """Преобразование в CodeSlice разных исходных данных."""
        data = (
            # Корректные операторы, строки, числа, символы.
            ((1, 1, 'сказать'),                 (1, 1, 'say')),
            ((1, 9, '"Начало программы"'),      (1, 9, '"Начало программы"')),
            ((2, 1, 'ПЕРЕЙТИ_НА'),              (2, 1, 'move_to')),
            ((2, 11, 'шаг_вправо'),             (2, 11, 'step_right')),
            ((2, 13, 'step_left'),              (2, 13, 'step_left')),
            ((3, 1, 'step_up'),                 (3, 1, 'step_up')),
            ((3, 12, '1'),                      (3, 12, '1')),
            ((4, 9, 'Справа_Стена'),            (4, 9, 'is_right_wall')),
            ((5, 10, 'is_green_tile'),          (5, 10, 'is_green_tile')),
            ((5, 29, 'красная_плитка'),         (5, 29, 'is_red_tile')),
            ((4, 1, 'пока'),                    (4, 1, 'while')),
            ((5, 5, 'если'),                    (5, 5, 'if')),
            ((5, 5, 'ИНАЧЕ'),                   (5, 5, 'else')),
            ((4, 6, 'not'),                     (4, 6, 'not')),
            ((5, 25, 'или'),                    (5, 25, 'or')),
            ((4, 22, '{'),                      (4, 22, '{')),
            ((5, 44, '}'),                      (5, 44, '}')),
            ((5, 47, '('),                      (5, 47, '(')),
            ((6, 2, 'put_red_tile'),            (6, 2, 'put_red_tile')),
            ((6, 5, 'положить_зелёную_плитку'), (6, 5, 'put_green_tile')),
            ((6, 9, 'убрать_плитку'),           (6, 9, 'remove_tile')),
            # Несуществующие операторы.
            ((1, 6, 'ещё'),                     (1, 6, 'ещё')),
            ((1, 10, 'зеленая_плита'),          (1, 10, 'зеленая_плита')),
            ((2, 1, 'шагай'),                   (2, 1, 'шагай')),
            ((2, 7, 'правее'),                  (2, 7, 'правее')),
            # Некорректные срезы.
            ((2, 7, ' шаг_вправо'),             (2, 7, ' шаг_вправо')),
            ((2, 7, '}\n'),                     (2, 7, '}\n')),
        )
        for source, expect in data:
            slice_ = lib_language.CodeSlice(*source)
            assert slice_.src_text == source[2]
            assert (slice_.line, slice_.pos, slice_.value) == expect


class Test2CodeSlicer:
    """Проверяет нарезателя текста."""

    def test_1_correct_data(self) -> None:
        """Корректные программы."""
        data: tuple = (
            {  # Простая программа. Разные операторы. Ошибок нет.
                'title': 'all_ok',
                'source': (
                    'Сказать "Начало программы"\n'
                    'перейти_на 1 1\n'
                    '# comment line\n'
                    'пока не справа_стена {\n'
                    '    если зелёная_плитка или красная_плитка {\n'
                    '        убрать_плитку положить_красную_плитку\n'
                    '    }\n'
                    '    иначе  # inline comment\n'
                    '    {\n'
                    '        убрать_плитку\n'
                    '    }\n'
                    '    шаг_вправо\n'
                    '}'
                ),
                'expect': [
                    (1, 1, 'Сказать'),
                    (1, 9, '"Начало программы"'),
                    (2, 1, 'перейти_на'),
                    (2, 12, '1'),
                    (2, 14, '1'),
                    (4, 1, 'пока'),
                    (4, 6, 'не'),
                    (4, 9, 'справа_стена'),
                    (4, 22, '{'),
                    (5, 5, 'если'),
                    (5, 10, 'зелёная_плитка'),
                    (5, 25, 'или'),
                    (5, 29, 'красная_плитка'),
                    (5, 44, '{'),
                    (6, 9, 'убрать_плитку'),
                    (6, 23, 'положить_красную_плитку'),
                    (7, 5, '}'),
                    (8, 5, 'иначе'),
                    (9, 5, '{'),
                    (10, 9, 'убрать_плитку'),
                    (11, 5, '}'),
                    (12, 5, 'шаг_вправо'),
                    (13, 1, '}'),
                ],
            }, {  # Пробелы и переносы строк в разном количестве.
                'title': 'many_spaces',
                'source': (
                    '\n'
                    '  перейти_на 1\n'
                    '     1\n'
                    'пока\n'
                    'не   справа_стена\n'
                    '{\n'
                    '    если\n'
                    '        зелёная_плитка {\n'
                    '        убрать_плитку\n'
                    '\n'
                    '\n'
                    '    }\n'
                    '     }\n'
                    '\n'
                    '\n'
                ),
                'expect': [
                    (2, 3, 'перейти_на'),
                    (2, 14, '1'),
                    (3, 6, '1'),
                    (4, 1, 'пока'),
                    (5, 1, 'не'),
                    (5, 6, 'справа_стена'),
                    (6, 1, '{'),
                    (7, 5, 'если'),
                    (8, 9, 'зелёная_плитка'),
                    (8, 24, '{'),
                    (9, 9, 'убрать_плитку'),
                    (12, 5, '}'),
                    (13, 6, '}'),
                ],
            }, {  # Несуществующий, но синтаксически корректный оператор.
                'title': 'unknown_valid_operator',
                'source': (
                    'пока ещё зеленая_плита {\n'
                    'шагай правее}\n'
                ),
                'expect': [
                    (1, 1, 'пока'),
                    (1, 6, 'ещё'),
                    (1, 10, 'зеленая_плита'),
                    (1, 24, '{'),
                    (2, 1, 'шагай'),
                    (2, 7, 'правее'),
                    (2, 13, '}'),
                ],
            }
        )
        for one in data:
            print(f'...{one["title"]}')
            expect = [lib_language.CodeSlice(*a) for a in one['expect']]
            slicer = lib_language.CodeSlicer()
            slicer.slice_text(one['source'])
            assert slicer.slices == expect

    def test_2_wrong_data(self) -> None:
        """Некорректные программы."""
        data: tuple = (
            {  # Ошибочный синтаксис оператора.
                'title': 'incorrect_operator',
                'source': 'шаг _вправо',
            }, {  # Ошибочная строка.
                'title': 'incorrect_quotes',
                'source': '\'шаг_вправо\'',
            }, {  # Ошибочная строка.
                'title': 'missing_quotes',
                'source': '"ша\n',
            }, {  # Ошибочный символ.
                'title': 'incorrect_character',
                'source': ';',
            }
        )
        for one in data:
            print(f'...{one["title"]}')
            slicer = lib_language.CodeSlicer()
            try:
                slicer.slice_text(one['source'])
                raise AssertionError('No exception.')
            except SyntaxError:
                pass


class Test3SimpleTree:
    """Проверяет построение дерева."""

    data: tuple = (
        # Простые условия без групп.
        {
            'title': 'cond',
            'source': ('is_right_wall',),
            'expect': ['is_right_wall']
        }, {
            'title': 'not',
            'source': ('not', 'is_right_wall',),
            'expect': ['not', ['is_right_wall']]
        }, {
            'title': 'or',
            'source': ('is_green_tile', 'or', 'is_red_tile',),
            'expect': ['or', ['is_green_tile'], ['is_red_tile']]
        }, {
            'title': 'not_and',
            'source': ('not', 'is_green_tile', 'and', 'is_red_tile',),
            'expect': ['and', ['not', ['is_green_tile']], ['is_red_tile']]
        }, {
            'title': 'not_and_not',
            'source': ('not', 'is_left_wall', 'and', 'not', 'is_right_wall',),
            'expect': ['and', ['not', ['is_left_wall']], ['not', ['is_right_wall']]]
        }, {
            'title': 'not_and_not_or',
            'source': (
                'not', 'is_left_wall',
                'and', 'not', 'is_right_wall',
                'or', 'is_red_tile',
            ),
            'expect': [
                'or',
                [
                    'and',
                    ['not', ['is_left_wall']],
                    ['not', ['is_right_wall']],
                ],
                ['is_red_tile'],
            ]
        }, {
            'title': 'not_and_not_or_and_and_and',
            'source': (
                'not', 'is_left_wall',
                'and', 'not', 'is_right_wall',
                'or', 'is_red_tile',
                'and', 'is_green_tile',
                'and', 'is_top_wall',
                'and', 'is_bottom_wall',
            ),
            'expect': [
                'and',
                [
                    'and',
                    [
                        'and',
                        [
                            'or',
                            [
                                'and',
                                ['not', ['is_left_wall']],
                                ['not', ['is_right_wall']],
                            ],
                            ['is_red_tile'],
                        ],
                        ['is_green_tile'],
                    ],
                    ['is_top_wall'],
                ],
                ['is_bottom_wall'],
            ]
        }
    )

    def test_1_making_tree(self) -> None:
        """Из корректных условий."""
        for one in self.__class__.data:
            print(f'...{one["title"]}')
            source = convert_to_slices(one['source'])
            expect = convert_to_slices(one['expect'])
            simple_tree = lib_language.SimpleTree()
            for slice_ in source:
                if slice_.value in ('and', 'or'):
                    simple_tree.root(slice_)
                else:
                    simple_tree.branch(slice_)
            assert simple_tree.tree == expect


class Test4ConditionASTBuilder:
    """Проверяет сборку АСД условий."""

    no_groups_data: tuple = (
        # Простые условия без групп.
        {
            'title': 'cond',
            'source': ('is_right_wall',),
            'expect': ['is_right_wall']
        }, {
            'title': 'not',
            'source': ('not', 'is_right_wall',),
            'expect': ['not', ['is_right_wall']]
        }, {
            'title': 'or',
            'source': ('is_green_tile', 'or', 'is_red_tile',),
            'expect': ['or', ['is_green_tile'], ['is_red_tile']]
        }, {
            'title': 'not_and',
            'source': ('not', 'is_green_tile', 'and', 'is_red_tile',),
            'expect': ['and', ['not', ['is_green_tile']], ['is_red_tile']]
        }, {
            'title': 'not_and_not',
            'source': ('not', 'is_left_wall', 'and', 'not', 'is_right_wall',),
            'expect': ['and', ['not', ['is_left_wall']], ['not', ['is_right_wall']]]
        }, {
            'title': 'not_and_not_or',
            'source': (
                'not', 'is_left_wall',
                'and', 'not', 'is_right_wall',
                'or', 'is_red_tile',
            ),
            'expect': [
                'or',
                [
                    'and',
                    ['not', ['is_left_wall']],
                    ['not', ['is_right_wall']],
                ],
                ['is_red_tile'],
            ]
        }, {
            'title': 'not_and_not_or_and_and_and',
            'source': (
                'not', 'is_left_wall',
                'and', 'not', 'is_right_wall',
                'or', 'is_red_tile',
                'and', 'is_green_tile',
                'and', 'is_top_wall',
                'and', 'is_bottom_wall',
            ),
            'expect': [
                'and',
                [
                    'and',
                    [
                        'and',
                        [
                            'or',
                            [
                                'and',
                                ['not', ['is_left_wall']],
                                ['not', ['is_right_wall']],
                            ],
                            ['is_red_tile'],
                        ],
                        ['is_green_tile'],
                    ],
                    ['is_top_wall'],
                ],
                ['is_bottom_wall'],
            ]
        }
    )

    def test_1_no_groups(self) -> None:
        """Из корректных условий без групп."""
        for one in self.__class__.no_groups_data:
            print(f'...{one["title"]}')
            source = convert_to_slices(one['source'])
            expect = convert_to_slices(one['expect'])
            ast = lib_language.ConditionASTBuilder()
            for slice_ in source:
                ast.next_slice(slice_)
            assert ast.tree == expect

    groups_data: tuple = (
        # Простые условия с группами.
        {
            'title': 'cond',
            'source': ('(', 'is_right_wall', ')',),
            'expect': ['is_right_wall']
        }, {
            'title': 'many_groups',
            'source': ('(', '(', '(', 'is_top_wall', ')', ')', ')',),
            'expect': ['is_top_wall']
        }, {
            'title': 'not',
            'source': ('not', '(', 'is_right_wall', ')',),
            'expect': ['not', ['is_right_wall']]
        }, {
            'title': 'or',
            'source': ('is_green_tile', 'or', '(', 'is_red_tile', ')',),
            'expect': ['or', ['is_green_tile'], ['is_red_tile']]
        }, {
            'title': 'not_and',
            'source': ('not', '(', 'is_green_tile', 'and', 'is_red_tile', ')',),
            'expect': ['not', ['and', ['is_green_tile'], ['is_red_tile']]]
        }, {
            'title': 'not_and_not',
            'source': ('not', 'is_left_wall', 'and', '(', 'not', 'is_right_wall', ')',),
            'expect': ['and', ['not', ['is_left_wall']], ['not', ['is_right_wall']]]
        }, {
            'title': 'not_and_not_or',
            'source': (
                'not', '(', 'is_left_wall', 'and', 'not', 'is_right_wall', ')', 'or', 'is_red_tile',
            ),
            'expect': [
                'or',
                [
                    'not',
                    [
                        'and',
                        ['is_left_wall'],
                        ['not', ['is_right_wall']],
                    ]
                ],
                ['is_red_tile'],
            ]
        }, {
            'title': 'not_and_not_or_and_and_and',
            'source': (
                'not',
                '(',
                    'is_left_wall',
                    'and',
                    '(',
                        'not',
                        '(',
                            'is_right_wall', 'or', 'is_red_tile',
                        ')',
                        'and', 'is_green_tile',
                        'and', 'is_top_wall',
                    ')',
                    'and', 'is_bottom_wall',
                ')',
            ),
            'expect': [
                'not',
                [
                    'and',
                    [
                        'and',
                        ['is_left_wall'],
                        [
                            'and',
                            [
                                'and',
                                [
                                    'not',
                                    [
                                        'or',
                                        ['is_right_wall'],
                                        ['is_red_tile']
                                    ]
                                ],
                                ['is_green_tile']
                            ],
                            ['is_top_wall']
                        ]
                    ],
                    ['is_bottom_wall']
                ]
            ]
        }
    )

    def test_2_groups(self) -> None:
        """Из корректных условий с группами."""
        for one in self.__class__.no_groups_data:
            print(f'...{one["title"]}')
            source = convert_to_slices(one['source'])
            expect = convert_to_slices(one['expect'])
            ast = lib_language.ConditionASTBuilder()
            for slice_ in source:
                ast.next_slice(slice_)
            assert ast.tree == expect

    wrong_data: tuple = (
        # Условия с ошибками.
        {
            'title': 'cond',
            'source': ('(', '(', 'is_right_wall', ')',),
        }, {
            'title': 'not',
            'source': ('is_right_wall', 'not',),
        }, {
            'title': 'or',
            'source': ('is_green_tile', 'or', 'or', '(', 'is_red_tile', ')',),
        }, {
            'title': 'and',
            'source': ('and', '(', 'is_green_tile', 'and', 'is_red_tile', ')',),
        }, {
            'title': 'not_not_not',
            'source': ('not', 'is_left_wall', 'not', '(', 'not', 'is_right_wall', ')',),
        }
    )

    def test_3_errors(self) -> None:
        """Из условий с ошибками."""
        for one in self.__class__.wrong_data:
            print(f'...{one["title"]}')
            source = convert_to_slices(one['source'])
            ast = lib_language.ConditionASTBuilder()
            try:
                for slice_ in source:
                    ast.next_slice(slice_)
                ast.check()
                raise AssertionError('No exception.')
            except SyntaxError:
                pass


class Test5CodeASTBuilder:
    """Проверяет построение АСД программы."""

    # Простая программа без ошибок.
    correct_data: tuple = (
        {
            'title': 'no_blocks',
            'source': [
                'say',
                '"Начало программы"',
                'move_to',
                '1',
                '1',
                'remove_tile',
                'put_red_tile',
                'step_right',
            ],
            'expect': [
                ['say', 'Начало программы'],
                ['move_to', 1, 1],
                ['remove_tile'],
                ['put_red_tile'],
                ['step_right']
            ],
        }, {
            'title': 'with_blocks',
            'source': [
                'while',
                'not',
                'is_right_wall',
                '{',
                'if',
                'is_green_tile',
                'или',
                'is_red_tile',
                '{',
                'remove_tile',
                'put_red_tile',
                '}',
                'else',
                '{',
                'remove_tile',
                '}',
                'step_right',
                '}',
            ],
            'expect': [
                [
                    'while',
                    ['not', ['is_right_wall']],
                    [
                        [
                            'if',
                            ['or', ['is_green_tile'], ['is_red_tile']],
                            [['remove_tile'], ['put_red_tile']],
                            [['remove_tile']],
                        ],
                        ['step_right']
                    ],
                ],
            ],
        }
    )

    def test_1_correct_data(self) -> None:
        """Программа без ошибок."""
        for one in self.__class__.correct_data:
            print(f'...{one["title"]}')
            source = convert_to_slices(one['source'])
            expect = convert_to_slices(one['expect'])
            ast = lib_language.CodeASTBuilder()
            for slice_ in source:
                ast.next_slice(slice_)
            ast.check()
            assert ast.tree == expect

    # Программа с ошибками синтаксиса.
    wrong_syntax_data: tuple = (
        {
            'title': 'not_complete_say',
            'source': ['say']
        }, {
            'title': 'not_complete_move',
            'source': ['move_to', '7']
        }, {
            'title': 'not_complete_while',
            'source': ['while', 'is_red_tile']
        }, {
            'title': 'not_complete_block',
            'source': ['while', 'is_red_tile', '{', 'remove_tile']
        }, {
            'title': 'bad_else',
            'source': ['remove_tile', 'else']
        }, {
            'title': 'bad_block',
            'source': ['{']
        }
    )

    def test_2_syntax_errors(self) -> None:
        """Программа с ошибками синтаксиса."""
        for one in self.__class__.wrong_syntax_data:
            print(f'...{one["title"]}')
            source = convert_to_slices(one['source'])
            ast = lib_language.CodeASTBuilder()
            try:
                for slice_ in source:
                    ast.next_slice(slice_)
                ast.check()
                raise AssertionError('No exception.')
            except SyntaxError:
                pass

    # Программа с ошибками значений.
    wrong_value_data: tuple = (
        {
            'title': 'bad_string',
            'source': ['say', 'Начало программы"']
        }, {
            'title': 'bad_number',
            'source': ['move_to', 'x']
        }, {
            'title': 'bad_number2',
            'source': ['move_to', '5', 'x']
        }
    )

    def test_3_value_errors(self) -> None:
        """Программа с ошибками значений."""
        for one in self.__class__.wrong_value_data:
            print(f'...{one["title"]}')
            source = convert_to_slices(one['source'])
            ast = lib_language.CodeASTBuilder()
            try:
                for slice_ in source:
                    ast.next_slice(slice_)
                ast.check()
                raise AssertionError('No exception.')
            except ValueError:
                pass


class Test6Interpreter:
    """Проверяет интерпретатор."""

    # Программы без ошибок.
    correct_data: tuple = (
        {
            'title': 'simple',
            'source': (
                'Сказать "Начало программы"\n'
                '# comment\n'
                'перейти_на 1 1\n'
                'убрать_плитку положить_красную_плитку\n'
                'шаг_вправо\n'
            ),
            'expect': [
                ['say', 'Начало программы'],
                ['move_to', 1, 1],
                ['remove_tile'],
                ['put_red_tile'],
                ['step_right']
            ],
        }, {
            'title': 'with_blocks',
            'source': (
                'пока не справа_стена\n'
                '{\n'
                '  если зелёная_плитка или красная_плитка\n'
                '  {\n'
                '    убрать_плитку\n'
                '    положить_красную_плитку\n'
                '  }\n'
                '  иначе\n'
                '  {\n'
                '    убрать_плитку\n'
                '  }\n'
                '  шаг_вправо\n'
                '}\n'
            ),
            'expect': [
                [
                    'while',
                    ['not', ['is_right_wall']],
                    [
                        [
                            'if',
                            ['or', ['is_green_tile'], ['is_red_tile']],
                            [['remove_tile'], ['put_red_tile']],
                            [['remove_tile']],
                        ],
                        ['step_right']
                    ],
                ],
            ],
        }
    )

    def test_1_no_errors(self) -> None:
        """Программы без ошибок."""
        for one in self.__class__.correct_data:
            print(f'...{one["title"]}')
            expect = convert_to_slices(one['expect'])
            program = lib_language.interpreter(one['source'])
            assert str(program) == str(expect)

    incorrect_data: tuple = (
        {
            'title': 'not_complete_say',
            'source': 'сказать'
        }, {
            'title': 'not_complete_move',
            'source': 'перейти_на 7'
        }, {
            'title': 'not_complete_while',
            'source': 'пока красная_плитка'
        }, {
            'title': 'not_complete_block',
            'source': 'пока is_red_tile { remove_tile'
        }, {
            'title': 'bad_else',
            'source': 'убрать_плитку иначе'
        }, {
            'title': 'bad_block',
            'source': '{'
        }, {
            'title': 'bad_string',
            'source': 'сказать Начало программы"'
        }, {
            'title': 'bad_number',
            'source': 'перейти_на x'
        }, {
            'title': 'bad_number2',
            'source': 'перейти_на 5 x'
        }
    )

    def test_2_errors(self) -> None:
        """Программы с ошибками."""
        for one in self.__class__.incorrect_data:
            print(f'...{one["title"]}')
            try:
                program = lib_language.interpreter(one['source'])
                raise AssertionError('Error was expected.')
            except SyntaxError:
                pass
            except ValueError:
                pass


class TypeClass:
    """Этот класс нужен для поиска тестовых классов."""
    def typefunc(self):
        """Этот метод нужен для поиска тестовых методов."""
        pass


if __name__.startswith('__main__'):
    # Собрать список тестовых юнитов.
    test_units = {}
    class_names = [a for a in dir() if a.startswith('Test')]
    class_names = filter(lambda a: eval(f'type({a}) == type(TypeClass)'), class_names)
    for class_name in class_names:
        method_names = [a for a in eval(f'dir({class_name})') if a.startswith('test_')]
        method_names = list(filter(
            lambda a: eval(f'type({class_name}.{a}) == type(TypeClass.typefunc)'), method_names
        ))
        if method_names:
            test_units[class_name] = list(method_names)

    # Выполнить все тестовые юниты.
    for class_name in sorted(test_units.keys()):
        for method_name in sorted(test_units[class_name]):
            print(f'Execute {class_name}.{method_name} ...')
            unit = eval(f'{class_name}()')
            eval(f'unit.{method_name}()')

    # Если исключений не возникло, то всё в порядке.
    print('Everything is ok.')
