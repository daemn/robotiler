from typing import Any
import lib
log = lib.Log('quiet')
__doc__ = """
Робот плиточник.

>>> robo = Robo(field)          # Создать робота на поле field.
>>> robo.load_program(program)  # Загрузить в робота программу.
>>> while True:
...     try:
...         robo.cpu_tact()     # Выполнять команды, пока не закончатся.
...     except ProgramEnded:
...         pass
"""


class RoboError(RuntimeError):
    """Базовая ошибка Робо."""
    def __init__(self, msg: str = 'Robo make something wrong.'):
        super().__init__(msg)


class HitTheWall(RoboError):
    """Робо ударился о стену."""
    def __init__(self, msg: str = 'Robo hit a wall.'):
        super().__init__(msg)


class BrokeTheTile(RoboError):
    """Робо сломал плитку."""
    def __init__(self, msg: str = 'Robo broke the tile.'):
        super().__init__(msg)


class BrokeTheTool(RoboError):
    """Робо сломал инструмент."""
    def __init__(self, msg: str = 'Robo broke the tool.'):
        super().__init__(msg)


class ProgramEnded(EOFError):
    """Программа закончилась."""
    def __init__(self, msg: str = 'Program ended.'):
        super().__init__(msg)


class Robo:
    """Робот плиточник."""
    x: int
    y: int
    field: Any
    program: list
    _stack: list
    _branch: list
    _pos: int

    def __init__(self, field: Any) -> None:
        """
        :param field: Поле с плитками.
        """
        self.field = field

    def load_program(self, program: list) -> None:
        """
        Загружает программу в формате абстрактного дерева в память Робо.

        :param program: Программа."""
        if not program:
            raise ValueError('Cannot load empty program.')
        self.program = program
        self.cpu_reset()

    def cpu_reset(self) -> None:
        """Сбрасывает исполнение программы."""
        self._stack = []
        self._branch = self.program
        self._pos = -1

    def cpu_tact(self) -> None:
        """Исполняет одну инструкцию программы."""
        cmd: list = self._get_next_cmd()
        {
            'say':              self._cmd_say,
            'move_to':          self._cmd_move_to,
            'step_right':       self._cmd_step_right,
            'step_left':        self._cmd_step_left,
            'step_up':          self._cmd_step_up,
            'step_down':        self._cmd_step_down,
            'put_red_tile':     self._cmd_put_red_tile,
            'put_green_tile':   self._cmd_put_green_tile,
            'remove_tile':      self._cmd_remove_tile,
            'if':               self._cmd_if,
            'while':            self._cmd_while,
        }[cmd[0].value](cmd)

    def _move_to(self, x: int, y: int) -> None:
        """Переставляет робо в указанные координаты.

        :param x: Координата X (колонка поля).
        :param y: Координата Y (строка поля).
        """
        if self.field.is_wall(x, y):
            self.field.dead_comes_to(self.x, self.y)
            raise HitTheWall
        self.field.robo_comes_to(x, y)
        self.x, self.y = x, y

    def _get_next_cmd(self) -> list:
        """Возвращает следующую команду программы."""
        self._pos += 1
        while True:
            try:
                return self._branch[self._pos]
            except IndexError:
                pass
            try:
                self._branch, self._pos = self._stack.pop()
            except IndexError as err:
                raise ProgramEnded() from err
            if self._branch[self._pos][0].value != 'while':
                self._pos += 1

    def _cmd_say(self, cmd: list) -> None:
        """Произносит сообщение."""
        print('[ROBO]', cmd[1].value)
        self.field.show_message(cmd[1].value)

    def _cmd_move_to(self, cmd: list) -> None:
        """Переставляет робо в указанные координаты."""
        x: int = cmd[1].value
        y: int = cmd[2].value
        log.debug(f'Robo move to {x},{y}')
        self._move_to(x, y)

    def _cmd_step_up(self, _: list) -> None:
        """Сдвигает робо вверх на одну клетку."""
        log.debug('Robo step up.')
        self._move_to(self.x, self.y - 1)

    def _cmd_step_down(self, _: list) -> None:
        """Сдвигает робо вниз на одну клетку."""
        log.debug('Robo step down.')
        self._move_to(self.x, self.y + 1)

    def _cmd_step_left(self, _: list) -> None:
        """Сдвигает робо влево на одну клетку."""
        log.debug('Robo step left.')
        self._move_to(self.x - 1, self.y)

    def _cmd_step_right(self, _: list) -> None:
        """Сдвигает робо вправо на одну клетку."""
        log.debug('Robo step right.')
        self._move_to(self.x + 1, self.y)

    def _cmd_put_red_tile(self, _: list) -> None:
        """Кладёт красную плитку."""
        log.debug('Robo put a red tile.')
        if self.field.is_empty(self.x, self.y):
            self.field.put_red_tile(self.x, self.y)
        else:
            raise BrokeTheTile

    def _cmd_put_green_tile(self, _: list) -> None:
        """Кладёт зелёную плитку."""
        log.debug('Robo put a green tile.')
        if self.field.is_empty(self.x, self.y):
            self.field.put_green_tile(self.x, self.y)
        else:
            raise BrokeTheTile

    def _cmd_remove_tile(self, _: list) -> None:
        """Убирает плитку."""
        log.debug('Robo remove a tile.')
        if self.field.is_red(self.x, self.y) or self.field.is_green(self.x, self.y):
            self.field.remove_wall_or_tile(self.x, self.y)
        else:
            raise BrokeTheTool

    def _cmd_if(self, cmd: list) -> None:
        """Условная конструкция 'if'"""
        log.debug('Robo if.')
        if self._test_condition(cmd[1]):
            self._stack.append((self._branch, self._pos))
            self._branch = cmd[2]
            self._pos = -1
        else:
            if len(cmd) == 4:
                self._stack.append((self._branch, self._pos))
                self._branch = cmd[3]
                self._pos = -1

    def _cmd_while(self, cmd: list) -> None:
        """Цикл 'while'"""
        log.debug('Robo while.')
        if self._test_condition(cmd[1]):
            self._stack.append((self._branch, self._pos))
            self._branch = cmd[2]
            self._pos = -1

    def _test_condition(self, condition: list) -> bool:
        """
        Возвращает результат условной конструкции.

        :param condition: Условие в виде дерева.
        """
        return {
            'not':              self._condition_not,
            'or':               self._condition_or,
            'and':              self._condition_and,
            'is_red_tile':      self._condition_is_red_tile,
            'is_green_tile':    self._condition_is_green_tile,
            'is_top_wall':      self._condition_is_top_wall,
            'is_bottom_wall':   self._condition_is_bottom_wall,
            'is_left_wall':     self._condition_is_left_wall,
            'is_right_wall':    self._condition_is_right_wall,
        }[condition[0].value](condition)

    def _condition_not(self, condition: list) -> bool:
        """Логическое 'не'."""
        return not self._test_condition(condition[1])

    def _condition_or(self, condition: list) -> bool:
        """Логическое 'или'."""
        return (self._test_condition(condition[1])
                or self._test_condition(condition[2]))

    def _condition_and(self, condition: list) -> bool:
        """Логическое 'и'."""
        return (self._test_condition(condition[1])
                and self._test_condition(condition[2]))

    def _condition_is_red_tile(self, _: list) -> bool:
        """Проверяет красную плитку под робо."""
        return self.field.is_red(self.x, self.y)

    def _condition_is_green_tile(self, _: list) -> bool:
        """Проверяет зелёную плитку под робо."""
        return self.field.is_green(self.x, self.y)

    def _condition_is_top_wall(self, _: list) -> bool:
        """Проверяет наличие стены сверху."""
        return self.field.is_wall(self.x, self.y - 1)

    def _condition_is_bottom_wall(self, _: list) -> bool:
        """Проверяет наличие стены снизу."""
        return self.field.is_wall(self.x, self.y + 1)

    def _condition_is_left_wall(self, _: list) -> bool:
        """Проверяет наличие стены слева."""
        return self.field.is_wall(self.x - 1, self.y)

    def _condition_is_right_wall(self, _: list) -> bool:
        """Проверяет наличие стены справа."""
        return self.field.is_wall(self.x + 1, self.y)
