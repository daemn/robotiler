from browser import document, html, window
from typing import Any, Callable, Optional
from time import time
import lib
__doc__ = """
Поле для плиток.

Для создания поля используйте класс HTMLField.
>>> field = HTMLField(**params)     # Инициализировать поле.
>>> field.use_click_handler(func)   # Указать функцию для обработки кликов.
>>> field.draw()                    # Прорисовать поле на экране.
"""
log = lib.Log('quiet')


class FieldStorage(dict):
    """
    Хранилище ячеек поля.
    Используется как словарь, в котором ключи -- это координаты на поле, а
    значения -- состояния ячеек.
    Ключ нужно указывать в виде кортежа (3, 4).
    Запись значения 'empty' равносильна удалению ячейки.

    Да, да, можно же использовать dict.get или collections.defaultdict для
    большего понта, но, как написано в README, это приложение я пишу для
    удовольствия, а не для эффективности.
    """

    def __getitem__(self, key: tuple) -> str:
        # return self.get(key, 'empty')
        try:
            return super().__getitem__(key)
        except KeyError:
            return 'empty'

    def __setitem__(self, key: tuple, value: str) -> None:
        if value == 'empty':
            self.__delitem__(key)
        else:
            super().__setitem__(key, value)

    def __delitem__(self, key: tuple) -> None:
        try:
            super().__delitem__(key)
        except KeyError:
            pass


class HTMLField:
    """Поле с плитками. Рисуется с помощью HTML."""
    steps: list
    cells_storage: FieldStorage
    cols: int
    rows: int
    container: Any
    click_handler: Optional[Callable]
    symbols: dict

    # class attribute
    _messages: list = []

    def __init__(
        self,
        container: Any,
        cols: int = 10,
        rows: int = 10,
    ) -> None:
        """
        :param container: Контейнер для поля в DOM.
        :param cols: Размер поля по ширине.
        :param rows: Размер поля по высоте.
        """

        log.debug(f'New HTML field ({cols}x{rows}) init.')
        self.container = container
        self.cols, self.rows = cols, rows
        self.cells_storage = FieldStorage()
        self.click_handler = self.fake_click_handler
        self.symbols = {
            'robo': '🤖',
            'dead': '☠',
            'step': '·',
        }

    @staticmethod
    def fake_click_handler(_) -> None:
        pass

    def use_click_handler(self, func: Callable) -> None:
        """
        Использовать этот обработчик кликов по ячейкам поля.

        :param func: Обработчик кликов.
        """
        self.click_handler = func

    def draw(self) -> None:
        """Сбрасывает поле в запомненное состояние и рисует на экране."""
        self.steps = []

        log.debug('Draw HTML field.')
        self.container.clear()
        self.container.width = (self.cols + 1) * 20
        self.container.height = (self.rows + 1) * 20

        # Нарисовать номера столбцов.
        for col in range(1, self.cols + 1):
            self._draw_col_num(col)

        # Нарисовать номера строк.
        for row in range(1, self.rows + 1):
            self._draw_row_num(row)

        # Нарисовать ячейки поля.
        for col in range(1, self.cols + 1):
            for row in range(1, self.rows + 1):
                self._draw_cell(col, row)

        # Восстановить запомненное состояние поля.
        for (col, row), cell_state in self.cells_storage.items():
            self._set_cell_state(col, row, cell_state)

    def add_row(self) -> None:
        """Добавляет к полю строку."""
        log.debug('Add one row to HTML field.')
        self.rows += 1
        self.container.height = (self.rows + 1) * 20
        row = self.rows

        # Нарисовать номер строки.
        self._draw_row_num(row)

        # Нарисовать ячейки строки.
        for col in range(1, self.cols + 1):
            self._draw_cell(col, row)

    def rm_row(self) -> None:
        """Удаляет строку."""
        if self.rows < 2:
            return
        row = self.rows
        log.debug('Remove one row from HTML field.')

        # Удалить номер строки.
        del document[self._row_num_id(row)]

        # Удалить ячейки.
        for col in range(1, self.cols + 1):
            del document[self._cell_id(col, row)]
        self.rows -= 1
        self.container.height = (self.rows + 1) * 20

    def add_col(self) -> None:
        """Добавляет к полю колонку."""
        log.debug('Add one column to HTML field.')
        self.cols += 1
        self.container.width = (self.cols + 1) * 20
        col = self.cols

        # Нарисовать номер столбца.
        self._draw_col_num(col)

        # Нарисовать ячейки столбца.
        for row in range(1, self.rows + 1):
            self._draw_cell(col, row)

    def rm_col(self) -> None:
        """Удаляет колонку."""
        if self.cols < 2:
            return
        col = self.cols
        log.debug('Remove one column from HTML field.')

        # Удалить номер колонки.
        del document[self._col_num_id(col)]

        # Удалить ячейки.
        for row in range(1, self.rows + 1):
            del document[self._cell_id(col, row)]
        self.cols -= 1
        self.container.width = (self.cols + 1) * 20

    def is_red(self, col: int, row: int) -> bool:
        """
        Проверяет наличие красной плитки.

        :param col: Колонка.
        :param row: Строка.
        :return: True, если плитка красная.
        """
        return self._get_cell_state(col, row) == 'red'

    def is_green(self, col: int, row: int) -> bool:
        """
        Проверяет наличие зелёной плитки.

        :param col: Колонка.
        :param row: Строка.
        :return: True, если плитка зелёная.
        """
        return self._get_cell_state(col, row) == 'green'

    def is_wall(self, col: int, row: int) -> bool:
        """
        Проверяет наличие стены (ячейки за пределами поля тоже считаются стеной).

        :param col: Колонка.
        :param row: Строка.
        :return: True, если стена.
        """
        if (
            col < 1
            or col > self.cols
            or row < 1
            or row > self.rows
        ):
            return True
        return self._get_cell_state(col, row) == 'wall'

    def is_empty(self, col: int, row: int) -> bool:
        """
        Проверяет пустую ячейку.

        :param col: Колонка.
        :param row: Строка.
        :return: True, если пустая ячейка.
        """
        return self._get_cell_state(col, row) == 'empty'

    def build_wall(self, col: int, row: int) -> None:
        """
        Рисует на поле стену.

        :param col: Колонка.
        :param row: Строка.
        """
        log.debug(f'build wall at {row},{col}.')
        self._set_cell_state(col, row, 'wall')

    def put_red_tile(self, col: int, row: int) -> None:
        """
        Кладёт красную плитку.

        :param col: Колонка.
        :param row: Строка.
        """
        log.debug('Put red tile to {},{}.'.format(col, row))
        self._set_cell_state(col, row, 'red')

    def put_green_tile(self, col: int, row: int) -> None:
        """
        Кладёт зелёную плитку.

        :param col: Колонка.
        :param row: Строка.
        """
        log.debug('Put green tile to {},{}.'.format(col, row))
        self._set_cell_state(col, row, 'green')

    def remove_wall_or_tile(self, col: int, row: int) -> None:
        """
        Убирает стену или плитку.

        :param col: Колонка.
        :param row: Строка.
        """
        log.debug('Remove wall or tile from {},{}.'.format(col, row))
        self._set_cell_state(col, row, 'empty')

    def dead_comes_to(self, col: int, row: int) -> None:
        """
        Рисует на этой ячейке мёртвого плиточника.

        :param col: Колонка.
        :param row: Строка.
        """
        log.debug('Dead comes to {},{}.'.format(col, row))
        try:
            self._draw_in_cell(
                self.steps[-1][0], self.steps[-1][1], self.symbols['step']
            )
        except IndexError:
            pass
        self.steps.append((col, row))
        self._draw_in_cell(col, row, self.symbols['dead'])

    def robo_comes_to(self, col: int, row: int) -> None:
        """
        Рисует на этой ячейке плиточника (ещё живого).

        :param col: Колонка.
        :param row: Строка.
        """
        log.debug('Robo comes to {},{}.'.format(col, row))
        try:
            self._draw_in_cell(
                self.steps[-1][0], self.steps[-1][1], self.symbols['step']
            )
        except IndexError:
            pass
        self.steps.append((col, row))
        self._draw_in_cell(col, row, self.symbols['robo'])

    def show_message(self, message: str) -> None:
        """
        Выводит сообщение робота над полем.

        :param message: Текстовое сообщение робота.
        """
        tag_div = html.DIV(message)
        tag_div.id = 'msg_' + str(time())
        x = y = len(HTMLField._messages)
        tag_div.top = x
        tag_div.left = y
        tag_div.setAttribute('class', 'message')
        self.container.attach(tag_div)
        HTMLField._messages.append(tag_div.id)
        window.setTimeout(self.hide_message, 3000)

    @staticmethod
    def hide_message() -> None:
        try:
            div_id = HTMLField._messages.pop(0)
            document[div_id].remove()
        except IndexError:
            pass

    def save_state(self) -> None:
        """Сохраняет текущее состояние ячеек поля в хранилище."""
        log.debug('Save current state of field to storage.')
        self.cells_storage = FieldStorage()
        for row in range(1, self.rows + 1):
            for col in range(1, self.cols + 1):
                self.cells_storage[(col, row)] = self._get_cell_state(col, row)

    def _draw_row_num(self, row: int) -> None:
        """Рисует номер строки."""
        tag_div = html.DIV(str(row))
        tag_div.id = self._row_num_id(row)
        tag_div.top = row * 20
        tag_div.left = 0
        tag_div.setAttribute('class', 'row_num')
        self.container.attach(tag_div)

    def _draw_col_num(self, col: int) -> None:
        """Рисует номер колонки."""
        tag_div = html.DIV(str(col))
        tag_div.id = self._col_num_id(col)
        tag_div.top = 0
        tag_div.left = col * 20
        tag_div.setAttribute('class', 'col_num')
        self.container.attach(tag_div)

    def _draw_cell(self, col: int, row: int) -> None:
        """Рисует ячейку."""
        tag_div = html.DIV()
        tag_div.id = self._cell_id(col, row)
        tag_div.top = row * 20
        tag_div.left = col * 20
        tag_div.setAttribute('class', 'empty')
        tag_div.bind('click', self.click_handler)
        self.container.attach(tag_div)

    @staticmethod
    def to_coords(cell_id: str) -> tuple:
        """
        Транслирует ID ячейки в координаты на поле.

        :param cell_id: ID ячейки.
        """
        col, row = tuple(cell_id[len('cell_'):].split(','))
        return int(col), int(row)

    @staticmethod
    def _row_num_id(row: int) -> str:
        """
        Собирает ID номера строки.

        :param row: Номер строки.
        :return: ID номера строки.
        """
        return f'row_{row}'

    @staticmethod
    def _col_num_id(col: int) -> str:
        """
        Собирает ID номера колонки.

        :param col: Номер колонки.
        :return: ID колонки.
        """
        return f'col_{col}'

    @staticmethod
    def _cell_id(col: int, row: int) -> str:
        """
        Собирает ID ячейки поля.

        :param col: Номер колонки.
        :param row: Номер строки.
        :return: ID ячейки.
        """
        return f'cell_{col},{row}'

    def _set_cell_state(self, col: int, row: int, state: str) -> None:
        """
        Устанавливает состояние ячейки.

        :param col: Колонка.
        :param row: Строка.
        :param state: Требуемое состояние.
        """
        log.debug(f'Set a cell {col},{row} state to {state}')
        document[self._cell_id(col, row)].setAttribute('class', state)

    def _get_cell_state(self, col: int, row: int) -> str:
        """
        Возвращает состояние ячейки.

        :param col: Колонка.
        :param row: Строка.
        :return: Состояние ячейки.
        """
        return document[self._cell_id(col, row)].getAttribute('class')

    def _draw_in_cell(self, col: int, row: int, symbol: str) -> None:
        """
        Устанавливает текстовое содержимое ячейки.

        :param col: Колонка.
        :param row: Строка.
        :param symbol: Символ для вставки в ячейку.
        """
        log.debug('Set a cell ({},{}) text to {}.'.format(col, row, symbol))
        document[self._cell_id(col, row)].text = symbol
