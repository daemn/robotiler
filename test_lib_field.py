# Так как модуль pytest и другие тестовые в браузере не импортируются, а модуль
# browser не импортируется вне браузера, пришлось закостылить такую систему.
# Чтобы сохранить порядок тестовых функций, приходится добавлять в имя
# номер для сортировки.

from browser import document, html
import lib_field
lib_field.log.level = 'quiet'


def new_field() -> lib_field.HTMLField:
    """Создаёт новый экземпляр поля для тестов."""
    field = lib_field.HTMLField(
        container=document['field_container'],
    )
    field.draw()
    return field


class Test1FieldStorage:
    """Проверяет класс типов данных FieldStorage."""

    def test_1_add_read_items(self) -> None:
        """Добавление и чтение данных."""
        storage = lib_field.FieldStorage()

        storage[(1, 4)] = 'red'
        storage[(3, 2)] = 'green'
        assert storage[(1, 4)] == 'red'
        assert storage[(3, 2)] == 'green'
        assert storage[(1, 2)] == 'empty'
        assert storage[(5, 6)] == 'empty'

    def test_2_delete_items(self) -> None:
        """Удаление данных."""
        storage = lib_field.FieldStorage()

        storage[(2, 2)] = 'green'
        storage[(3, 2)] = 'red'
        storage[(4, 2)] = 'empty'
        del storage[(3, 2)]
        assert dict(storage) == {(2, 2): 'green'}


class Test2HTMLField:
    """Проверяет класс рисования HTML-поля."""

    def test_1_making(self) -> None:
        """Создание поля с разными параметрами."""
        field = lib_field.HTMLField(
            container=document['field_container'],
        )
        field.draw()

        field = lib_field.HTMLField(
            container=document['field_container'],
            rows=5,
            cols=7,
        )

        def fake_click_handler(_):
            pass

        field.use_click_handler(fake_click_handler)
        field.draw()

    def test_2_resizing(self) -> None:
        """Изменение размера поля."""
        field = new_field()

        field.add_row()
        field.add_row()
        field.add_col()
        field.rm_col()
        field.rm_col()
        field.rm_row()

    def test_3_walls(self) -> None:
        """Постройка стен."""
        field = new_field()

        field.build_wall(2, 4)
        assert field.is_wall(2, 4) is True
        assert field.is_wall(3, 4) is False
        field.remove_wall_or_tile(2, 4)
        assert field.is_wall(2, 4) is False
        assert field.is_empty(2, 4) is True

    def test_4_tiles(self) -> None:
        """Раскладывание плиток."""
        field = new_field()

        field.put_red_tile(3, 5)
        assert field.is_red(3, 5) is True
        field.put_green_tile(3, 5)
        assert field.is_green(3, 5) is True
        field.remove_wall_or_tile(3, 5)
        assert field.is_green(3, 5) is False
        assert field.is_wall(3, 5) is False
        assert field.is_empty(3, 5) is True

    def test_5_draw(self) -> None:
        """Рисование символов на поле."""
        field = new_field()

        field.robo_comes_to(2, 2)
        field.robo_comes_to(3, 2)
        field.dead_comes_to(4, 2)

    def test_6_save_load_state(self) -> None:
        """Сохранение и восстановление состояния поля."""
        field = new_field()

        assert field.is_wall(1, 2) is False
        field.save_state()
        field.build_wall(1, 2)
        assert field.is_wall(1, 2) is True
        field.draw()
        assert field.is_wall(1, 2) is False

        field.build_wall(2, 2)
        field.put_red_tile(3, 3)
        field.put_green_tile(4, 4)
        field.save_state()
        field.remove_wall_or_tile(2, 2)
        field.remove_wall_or_tile(3, 3)
        field.remove_wall_or_tile(4, 4)
        assert field.is_empty(2, 2) is True
        assert field.is_empty(3, 3) is True
        assert field.is_empty(4, 4) is True
        field.draw()
        assert field.is_wall(2, 2) is True
        assert field.is_red(3, 3) is True
        assert field.is_green(4, 4) is True
        assert field.is_green(3, 3) is False

    def test_7_to_coords(self) -> None:
        """Трансляция ID ячейки в координаты."""
        field = new_field()

        assert field.to_coords('cell_4,8') == (4, 8)


class TypeClass:
    """Этот класс нужен для поиска тестовых классов."""
    def typefunc(self):
        """Этот метод нужен для поиска тестовых методов."""
        pass


if __name__.startswith('__main__'):
    # Собрать список тестовых юнитов.
    test_units = {}
    class_names = [a for a in dir() if a.startswith('Test')]
    class_names = filter(lambda a: eval(f'type({a}) == type(TypeClass)'), class_names)
    for class_name in class_names:
        method_names = [a for a in eval(f'dir({class_name})') if a.startswith('test_')]
        method_names = list(filter(
            lambda a: eval(f'type({class_name}.{a}) == type(TypeClass.typefunc)'), method_names
        ))
        if method_names:
            test_units[class_name] = list(method_names)

    # Выполнить все тестовые юниты.
    for class_name in sorted(test_units.keys()):
        for method_name in sorted(test_units[class_name]):
            print(f'Execute {class_name}.{method_name} ...')
            unit = eval(f'{class_name}()')
            eval(f'unit.{method_name}()')

    # Если исключений не возникло, то всё в порядке.
    print('Everything is ok.')
