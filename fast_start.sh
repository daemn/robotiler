#!/bin/bash
set -eu

WORK_DIR=$( dirname "$0" )
echo "WORK_DIR=${WORK_DIR}"
echo "ROBOTILER_PORT=${ROBOTILER_PORT:=80}"

cd "${WORK_DIR}"
docker build --pull -t robotiler:app --target app .
ContainerID=$( docker run --rm -d -p "${ROBOTILER_PORT}":80 robotiler:app )

google-chrome "http://localhost:${ROBOTILER_PORT}"

echo "Контейнер плиточника запущен. Не забудьте его остановить: docker stop ${ContainerID}"
