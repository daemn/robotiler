# Так как модуль pytest и другие тестовые в браузере не импортируются, а модуль
# browser не импортируется вне браузера, пришлось закостылить такую систему.
# Чтобы сохранить порядок тестовых функций, приходится добавлять в имя
# номер для сортировки.

from browser import document, html
import lib_field
import lib_robo
import lib_language
lib_field.log.level = 'quiet'
lib_robo.log.level = 'quiet'
lib_language.log.level = 'quiet'


def convert_to_slices(data: list) -> list:
    """
    Из дерева со строками, делает дерево со слайсами.

    :param data: Одно или многоуровневый список со строками.
    :return: Одно или многоуровневый список со слайсами.
    """
    result: list = []
    for note in data:
        if isinstance(note, str) or isinstance(note, int):
            result.append(lib_language.CodeSlice(1, 2, note))
        else:
            result.append(convert_to_slices(note))
    return result


def new_field() -> lib_field.HTMLField:
    """Создаёт новый экземпляр поля для тестов."""
    field = lib_field.HTMLField(
        container=document['field_container'],
    )
    field.draw()
    return field


class Test1Robo:
    """Проверяет Robo."""

    def test_1_making(self) -> None:
        """Создание Робо."""
        robo = lib_robo.Robo(new_field())


class Test2SimplePrograms:
    """Проверяет простые программы."""

    def test_1_say(self) -> None:
        """Произношение Робо."""
        robo = lib_robo.Robo(new_field())

        program = convert_to_slices([
            ['move_to', 2, 5], ['say', 'Hello, world!'],
        ])
        robo.load_program(program)
        try:
            for _ in range(100):
                robo.cpu_tact()
            raise AssertionError('Program hangup.')
        except lib_robo.ProgramEnded:
            pass

    def test_2_moving(self) -> None:
        """Перемещение Робо."""
        robo = lib_robo.Robo(new_field())

        program = convert_to_slices([
            ['move_to', 2, 5], ['step_up'], ['step_right'], ['step_down'], ['step_left'],
        ])
        robo.load_program(program)
        try:
            for _ in range(100):
                robo.cpu_tact()
            raise AssertionError('Program hangup.')
        except lib_robo.ProgramEnded:
            pass

    def test_3_put_remove_tiles(self) -> None:
        """Навыки Робо раскладывать и удалять плитку."""
        robo = lib_robo.Robo(new_field())

        program = convert_to_slices([
            ['move_to', 2, 5],
            ['put_red_tile'],
            ['step_up'],
            ['put_green_tile'],
            ['step_right'],
            ['put_green_tile'],
            ['step_down'],
            ['put_red_tile'],
            ['step_left'],
            ['remove_tile'],
            ['step_up'],
            ['remove_tile'],
        ])
        robo.load_program(program)
        try:
            for _ in range(100):
                robo.cpu_tact()
            raise AssertionError('Program hangup.')
        except lib_robo.ProgramEnded:
            pass


class Test3IfConditions:
    """Проверяет 'if' с условиями."""

    def test_1_walls(self) -> None:
        """Как Робо видит стены."""
        field = new_field()
        robo = lib_robo.Robo(field)

        field.build_wall(3, 3)
        field.build_wall(4, 3)
        field.build_wall(3, 5)

        program = convert_to_slices([
            ['move_to', 2, 4],
            [
                'if',
                ['not', ['and', ['is_top_wall'], ['is_bottom_wall']]],
                [['put_green_tile']],
                [['put_red_tile']]
            ],
            ['step_right'],
            [
                'if',
                ['or', ['is_top_wall'], ['is_bottom_wall']],
                [['put_green_tile'], ['step_right']]
            ],
            [
                'if',
                ['and', ['is_top_wall'], ['not', ['is_bottom_wall']]],
                [['put_green_tile'], ['step_down']],
                [['put_red_tile']]
            ],
            [
                'if',
                ['and', ['is_top_wall'], ['not', ['is_bottom_wall']]],
                [['put_green_tile']]
            ],
        ])
        robo.load_program(program)
        try:
            for _ in range(100):
                robo.cpu_tact()
            raise AssertionError('Program hangup.')
        except lib_robo.ProgramEnded:
            pass

        assert field.is_green(1, 4) is False
        assert field.is_green(2, 4) is True
        assert field.is_green(3, 4) is True
        assert field.is_green(4, 4) is True
        assert field.is_green(5, 4) is False

    def test_2_tiles(self) -> None:
        """Как Робо видит плитки."""
        field = new_field()
        robo = lib_robo.Robo(field)

        field.put_red_tile(3, 3)
        field.put_red_tile(4, 3)
        field.put_green_tile(5, 3)

        program = convert_to_slices([
            ['move_to', 3, 3],
            [
                'if',
                ['is_red_tile'],
                [['step_down'], ['put_green_tile'], ['step_up']],
                [['remove_tile']],
            ],
            ['step_right'],
            [
                'if',
                ['or', ['is_top_wall'], ['not', ['is_green_tile']]],
                [['step_down'], ['put_green_tile'], ['step_up']],
                [['remove_tile']],
            ],
            ['step_right'],
            [
                'if',
                ['or', ['is_red_tile'], ['not', ['is_green_tile']]],
                [['remove_tile']],
                [['step_down'], ['put_green_tile'], ['step_up']],
            ],
        ])
        robo.load_program(program)
        try:
            for _ in range(100):
                robo.cpu_tact()
            raise AssertionError('Program hangup.')
        except lib_robo.ProgramEnded:
            pass

        assert field.is_red(3, 3) is True
        assert field.is_red(4, 3) is True
        assert field.is_green(5, 3) is True
        assert field.is_green(3, 4) is True
        assert field.is_green(4, 4) is True
        assert field.is_green(5, 4) is True


class Test4While:
    """Проверяет цикл 'while'."""

    def test_1_while_walls(self) -> None:
        """Пройти по коридору."""
        field = new_field()
        robo = lib_robo.Robo(field)

        field.build_wall(3, 3)
        field.build_wall(4, 5)
        field.build_wall(5, 3)

        program = convert_to_slices([
            ['move_to', 3, 4],
            [
                'while',
                ['or', ['is_top_wall'], ['is_bottom_wall']],
                [['step_right']],
                [['step_down']]
            ],
            ['put_green_tile'],
        ])
        robo.load_program(program)
        try:
            for _ in range(100):
                robo.cpu_tact()
            raise AssertionError('Program hangup.')
        except lib_robo.ProgramEnded:
            pass

        assert field.is_green(6, 4) is True


class Test5Blocks:
    """Проверяет сложную программу с блоками в условии."""

    def test_1_right_to_wall(self) -> None:
        """Пройти вправо до стены."""
        field = new_field()
        robo = lib_robo.Robo(field)

        field.build_wall(7, 3)

        program = convert_to_slices([
            ['move_to', 4, 3],
            [
                'while',
                ['not', ['is_right_wall']],
                [
                    [
                        'if',
                        ['or', ['is_green_tile'], ['is_red_tile']],
                        [['remove_tile'], ['put_red_tile']],
                        [['put_green_tile']],
                    ],
                    ['step_right']
                ],
            ],
            ['step_down'],
            ['put_red_tile'],
        ])
        robo.load_program(program)
        try:
            for _ in range(100):
                robo.cpu_tact()
            raise AssertionError('Program hangup.')
        except lib_robo.ProgramEnded:
            pass

        assert field.is_green(4, 3) is True
        assert field.is_green(5, 3) is True
        assert field.is_red(6, 4) is True


class Test6Errors:
    """Проверяет программы с ошибками."""

    def test_1_hit_the_wall(self) -> None:
        """Удариться в стену."""
        field = new_field()
        robo = lib_robo.Robo(field)

        field.build_wall(3, 3)

        program = convert_to_slices([
            ['move_to', 2, 3],
            ['while', ['not', ['is_top_wall']], [['step_right']], []],
        ])
        robo.load_program(program)
        try:
            for _ in range(100):
                robo.cpu_tact()
            raise AssertionError('Program hangup.')
        except lib_robo.HitTheWall:
            pass
        except lib_robo.ProgramEnded as err:
            raise AssertionError('HitTheWall did not raised.') from err

    def test_2_broke_the_tile(self) -> None:
        """Сломать плитку."""
        field = new_field()
        robo = lib_robo.Robo(field)

        field.put_green_tile(3, 3)

        program = convert_to_slices([
            ['move_to', 3, 3],
            ['put_red_tile'],
        ])
        robo.load_program(program)
        try:
            for _ in range(100):
                robo.cpu_tact()
            raise AssertionError('Program hangup.')
        except lib_robo.BrokeTheTile:
            pass
        except lib_robo.ProgramEnded as err:
            raise AssertionError('BrokeTheTile did not raised.') from err

    def test_3_broke_the_tool(self) -> None:
        """Сломать инструмент."""
        field = new_field()
        robo = lib_robo.Robo(field)

        program = convert_to_slices([
            ['move_to', 3, 3],
            ['remove_tile'],
        ])
        robo.load_program(program)
        try:
            for _ in range(100):
                robo.cpu_tact()
            raise AssertionError('Program hangup.')
        except lib_robo.BrokeTheTool:
            pass
        except lib_robo.ProgramEnded as err:
            raise AssertionError('BrokeTheTool did not raised.') from err


class TypeClass:
    """Этот класс нужен для поиска тестовых классов."""
    def typefunc(self):
        """Этот метод нужен для поиска тестовых методов."""
        pass


if __name__.startswith('__main__'):
    # Собрать список тестовых юнитов.
    test_units = {}
    class_names = [a for a in dir() if a.startswith('Test')]
    class_names = filter(lambda a: eval(f'type({a}) == type(TypeClass)'), class_names)
    for class_name in class_names:
        method_names = [a for a in eval(f'dir({class_name})') if a.startswith('test_')]
        method_names = list(filter(
            lambda a: eval(f'type({class_name}.{a}) == type(TypeClass.typefunc)'), method_names
        ))
        if method_names:
            test_units[class_name] = list(method_names)

    # Выполнить все тестовые юниты.
    for class_name in sorted(test_units.keys()):
        for method_name in sorted(test_units[class_name]):
            print(f'Execute {class_name}.{method_name} ...')
            unit = eval(f'{class_name}()')
            eval(f'unit.{method_name}()')

    # Если исключений не возникло, то всё в порядке.
    print('Everything is ok.')
