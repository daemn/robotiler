from typing import Union, Callable, Optional
import lib
log = lib.Log('quiet')
__doc__ = """
Интерпретация программы плиточника и построение абстрактного синтаксического дерева.

>>> program = interpreter(source_code)
"""


class CodeSlice:
    """
    Срез исходного кода.

    Атрибуты:
    line, pos   -- Строка и позиция в строке найденного среза.
    src_text    -- Текст среза.
    value       -- Текст среза, если возможно, переведённый в универсальную форму.
    """
    line: int
    pos: int
    src_text: str
    value: Union[str, int]

    translate: dict = {
        'сказать':                  'say',
        'перейти_на':               'move_to',
        'шаг_вверх':                'step_up',
        'шаг_вниз':                 'step_down',
        'шаг_влево':                'step_left',
        'шаг_вправо':               'step_right',
        'убрать_плитку':            'remove_tile',
        'положить_красную_плитку':  'put_red_tile',
        'положить_зелёную_плитку':  'put_green_tile',
        'пока':                     'while',
        'если':                     'if',
        'иначе':                    'else',
        'сверху_стена':             'is_top_wall',
        'снизу_стена':              'is_bottom_wall',
        'справа_стена':             'is_right_wall',
        'слева_стена':              'is_left_wall',
        'зелёная_плитка':           'is_green_tile',
        'красная_плитка':           'is_red_tile',
        'не':                       'not',
        'или':                      'or',
        'и':                        'and',
    }

    def __init__(self, line: int, pos: int, src_text: Union[str, int]) -> None:
        """
        :param line: Строка в исходном коде.
        :param pos: Позиция в строке исходного кода.
        :param src_text: Текст среза.
        """
        self.line = line
        self.pos = pos
        self.src_text = src_text
        self.value = self.__class__.translate.get(str(src_text).lower(), src_text)

    def __eq__(self, other) -> bool:
        return (
                isinstance(other, self.__class__)
                and self.line == other.line
                and self.pos == other.pos
                and self.value == other.value
        )

    def __repr__(self) -> str:
        return str(self.value)


class CodeSlicer:
    """Нарезает программный код на ключевые слова, числа, символы..."""
    # Нарезатель получился какой-то громоздкий и неудобный, но мне, из интереса,
    # захотелось написать его конечным автоматом. Так что так...

    text: str
    slices: list
    _state: str
    _fsm_triggers: dict
    _states: dict

    def __init__(self) -> None:
        self._state = 'start'
        self.slices = []
        self._fsm_triggers = {
            'start': (
                (self._is_quote, 'string_begin'),
                (self._is_blocks, 'char'),
                (self._is_number, 'number_begin'),
                (self._is_alpha, 'word_begin'),
                (self._is_comment, 'comment'),
            ),
            'string_begin': (),
            'string': (
                (self._is_quote, 'string_end'),
            ),
            'string_end': (),
            'char': (),
            'comment': (
                (self._is_newline, 'start'),
            ),
            'word_begin': (),
            'word': (
                (self._is_blocks, 'char'),
                (self._is_space, 'start'),
            ),
            'number_begin': (),
            'number': (
                (self._is_blocks, 'char'),
                (self._is_space, 'start'),
            )
        }
        self._states = {
            'start':        self._state_start,
            'string_begin': self._state_string_begin,
            'string':       self._state_string,
            'string_end':   self._state_string_end,
            'char':         self._state_char,
            'comment':      self._state_comment,
            'word_begin':   self._state_word_begin,
            'word':         self._state_word,
            'number_begin': self._state_number_begin,
            'number':       self._state_number,
        }

    @staticmethod
    def _is_quote(char: str) -> bool:
        return char == '"'

    @staticmethod
    def _is_comment(char: str) -> bool:
        return char == '#'

    @staticmethod
    def _is_newline(char: str) -> bool:
        return char == '\n'

    @staticmethod
    def _is_blocks(char: str) -> bool:
        return char in '{}()'

    @staticmethod
    def _is_number(char: str) -> bool:
        return char in '0123456789'

    @staticmethod
    def _is_alpha(char: str) -> bool:
        return char.isalpha()

    @staticmethod
    def _is_word(char: str) -> bool:
        return char.isalpha() or char == '_'

    @staticmethod
    def _is_space(char: str) -> bool:
        return char.isspace()

    def slice_text(self, text: str) -> None:
        """
        Нарезает программный код на элементы.

        :param text: Код программы.
        """
        self._state = 'start'
        self.text = text
        self.slices = []

        line, pos = 1, 1
        for char in text:
            self.next_char(line, pos, char)
            if char == '\n':
                line += 1
                pos = 1
            else:
                pos += 1

    def next_char(self, line: int, pos: int, char: str) -> None:
        """
        Обрабатывает очередной символ.

        :param line: Строка на которой расположен символ.
        :param pos: Позиция символа в строке.
        :param char: Символ.
        """
        # Если в текущем состоянии, подойдёт триггер, то сменить состояние.
        for trigger in self._fsm_triggers[self._state]:
            if trigger[0](char):
                self._state = trigger[1]
                break

        # Отправить символ в обработчик текущего состояния.
        self._states[self._state](line, pos, char)

    def _state_start(self, line: int, pos: int, char: str) -> None:
        if not self._is_space(char):
            raise SyntaxError(f'Is not space "{char}" at line "{line}", pos "{pos}".')

    def _state_string_begin(self, line: int, pos: int, char: str) -> None:
        self.slices.append(CodeSlice(line, pos, char))
        self._state = 'string'

    def _state_string(self, line: int, pos: int, char: str) -> None:
        if char == '\n':
            raise SyntaxError(f'Unexpected end of string at line "{line}", pos "{pos}".')
        slice_: CodeSlice = self.slices.pop()
        line: int = slice_.line
        pos: int = slice_.pos
        string: str = slice_.src_text + char
        self.slices.append(CodeSlice(line, pos, string))

    def _state_string_end(self, line: int, pos: int, char: str) -> None:
        slice_: CodeSlice = self.slices.pop()
        line: int = slice_.line
        pos: int = slice_.pos
        string: str = slice_.src_text + char
        self.slices.append(CodeSlice(line, pos, string))
        self._state = 'start'

    def _state_char(self, line: int, pos: int, char: str) -> None:
        self.slices.append(CodeSlice(line, pos, char))
        self._state = 'start'

    def _state_comment(self, line: int, pos: int, char: str) -> None:
        pass

    def _state_word_begin(self, line: int, pos: int, char: str) -> None:
        self.slices.append(CodeSlice(line, pos, char))
        self._state = 'word'

    def _state_word(self, line: int, pos: int, char: str) -> None:
        if not self._is_word(char):
            raise SyntaxError(
                f'Is not allowed word near "{char}" at line "{line}", pos "{pos}".'
            )
        slice_: CodeSlice = self.slices.pop()
        line: int = slice_.line
        pos: int = slice_.pos
        string: str = slice_.src_text + char
        self.slices.append(CodeSlice(line, pos, string))

    def _state_number_begin(self, line: int, pos: int, char: str) -> None:
        self.slices.append(CodeSlice(line, pos, char))
        self._state = 'number'

    def _state_number(self, line: int, pos: int, char: str) -> None:
        if not self._is_number(char):
            raise SyntaxError(
                f'Is not number near "{char}" at line "{line}", pos "{pos}".'
            )
        slice_: CodeSlice = self.slices.pop()
        line: int = slice_.line
        pos: int = slice_.pos
        string: str = slice_.src_text + char
        self.slices.append(CodeSlice(line, pos, string))


class SimpleTree:
    """Строит простое дерево."""
    tree: list
    _current_branch: Optional[list]

    def __init__(self) -> None:
        self.tree = []
        self._current_branch = None

    def __eq__(self, other) -> bool:
        return (
            isinstance(other, self.__class__)
            and self.tree == other.tree
        )

    def root(self, slice_: CodeSlice) -> None:
        """
        Сделать текущее дерево веткой нового корня.

        :param slice_: Имя корня.
        """
        self.tree = [slice_, self.tree]
        self._current_branch = self.tree

    def branch(self, slice_: CodeSlice) -> None:
        """
        Добавить новую ветку.

        :param slice_: Имя ветки.
        """
        try:
            self._current_branch.append([slice_])
            self._current_branch = self._current_branch[-1]
        except AttributeError:
            self.tree = [slice_]
            self._current_branch = self.tree

    def leaf(self, leaf: Union[list, CodeSlice]) -> None:
        """
        Добавить лист на крайнюю ветку.

        :param leaf: Содержимое листа.
        """
        try:
            self._current_branch.append(leaf)
        except AttributeError:
            if isinstance(leaf, list):
                self.tree = leaf
            else:
                self.tree = [leaf]
            self._current_branch = self.tree


class ConditionASTBuilder:
    """
    Строит Абстрактное Синтаксическое Дерево условных конструкций.

        Attributes:
    tree    -- Дерево условий.

        Methods:
    next_slice  -- Обрабатывает один срез исходного кода.
    Возвращает False, когда встречает конец группы и больше продолжать нельзя.
    """

    tree: list
    next_slice: Callable
    _line: int
    _pos: int

    _conditions: tuple = (
        'is_top_wall',
        'is_bottom_wall',
        'is_right_wall',
        'is_left_wall',
        'is_green_tile',
        'is_red_tile',
    )

    def __init__(self) -> None:
        self._tree = SimpleTree()
        self.next_slice = self._state_start

    @property
    def tree(self) -> list:
        return self._tree.tree

    def check(self) -> None:
        """
        Корректно ли обработанное условное выражение?
        Если нет, то вызывает исключение SyntaxError.
        """
        if (
                self.next_slice != self._state_complete
                and self.next_slice != self._state_finish
        ):
            raise SyntaxError(
                'Condition started at line: {}, pos: {} is not correct.'
                .format(self._line, self._pos)
            )

    def _state_start(self, code_slice: CodeSlice) -> bool:
        self._line, self._pos = code_slice.line, code_slice.pos
        if code_slice.value == 'not':
            self._tree.branch(code_slice)
            self.next_slice = self._state_not
        elif code_slice.value == '(':
            self._child_ast = ConditionASTBuilder()
            self.next_slice = self._state_grp
        elif code_slice.value in self.__class__._conditions:
            self._tree.branch(code_slice)
            self.next_slice = self._state_complete
        else:
            raise SyntaxError(
                'Unexpected "{}" in line: {}, pos: {}.'
                .format(code_slice.src_text, code_slice.line, code_slice.pos)
            )
        return True

    def _state_not(self, code_slice: CodeSlice) -> bool:
        if code_slice.value == '(':
            self._child_ast = ConditionASTBuilder()
            self.next_slice = self._state_grp
        elif code_slice.value in self.__class__._conditions:
            self._tree.branch(code_slice)
            self.next_slice = self._state_complete
        else:
            raise SyntaxError(
                'Condition or group expected, but found "{}" in line: {}, pos: {}.'
                .format(code_slice.src_text, code_slice.line, code_slice.pos)
            )
        return True

    def _state_and_or(self, code_slice: CodeSlice) -> bool:
        if code_slice.value == 'not':
            self._tree.branch(code_slice)
            self.next_slice = self._state_not
        elif code_slice.value == '(':
            self._child_ast = ConditionASTBuilder()
            self.next_slice = self._state_grp
        elif code_slice.value in self.__class__._conditions:
            self._tree.branch(code_slice)
            self.next_slice = self._state_complete
        else:
            raise SyntaxError(
                'Condition or group expected, but found "{}" in line: {}, pos: {}.'
                .format(code_slice.src_text, code_slice.line, code_slice.pos)
            )
        return True

    def _state_complete(self, code_slice: CodeSlice) -> bool:
        if code_slice.value in ('and', 'or'):
            self._tree.root(code_slice)
            self.next_slice = self._state_and_or
        elif code_slice.value == ')':
            self.next_slice = self._state_finish
            return False
        else:
            raise SyntaxError(
                '"and", "or" or group ending expected, but found "{}" in line: {}, pos: {}.'
                .format(code_slice.src_text, code_slice.line, code_slice.pos)
            )
        return True

    def _state_grp(self, code_slice: CodeSlice) -> bool:
        if self._child_ast.next_slice(code_slice):
            return True
        self._child_ast.check()
        self._tree.leaf(self._child_ast.tree)
        self.next_slice = self._state_complete
        return True

    def _state_finish(self, _) -> None:
        raise RuntimeError('Attempting to use the machine in a finish state.')


class CodeASTBuilder:
    """
    Строит Абстрактное Синтаксическое Дерево исполняемого кода.

        Attributes:
    tree    -- Дерево кода.

        Methods:
    next_slice  -- Обрабатывает один срез исходного кода.
    Возвращает False, когда встречает конец блока и больше продолжать нельзя.
    """

    tree: list
    next_slice: Callable
    _line: int
    _pos: int

    def __init__(self) -> None:
        self.tree = []
        self.next_slice = self._state_start

    def check(self) -> None:
        """
        Корректен ли обработанный блок кода?
        Если нет, то вызывает исключение SyntaxError.
        """
        if (
                self.next_slice != self._state_start
                and self.next_slice != self._state_finish
        ):
            raise SyntaxError(
                'Block of code is not correct near the line: {}, pos: {}.'
                .format(self._line, self._pos)
            )

    def _state_start(self, code_slice: CodeSlice) -> bool:
        self._line, self._pos = code_slice.line, code_slice.pos
        value: str = code_slice.value
        if value == 'say':
            self.tree.append([code_slice])
            self.next_slice = self._state_say_arg
        elif value == 'move_to':
            self.tree.append([code_slice])
            self.next_slice = self._state_move_to_arg1
        elif value == 'if':
            self.tree.append([code_slice])
            self._condition = ConditionASTBuilder()
            self.next_slice = self._state_condition
        elif value == 'else':
            if not self.tree or self.tree[-1][0].value != 'if':
                raise SyntaxError(
                    '"else" without "if" at line: {}, pos: {}.'
                    .format(code_slice.line, code_slice.pos)
                )
            self._child_ast = CodeASTBuilder()
            self.next_slice = self._state_else_prepare
        elif value == '}':
            self.next_slice = self._state_finish
            return False
        elif value in (
            'step_up', 'step_down', 'step_left', 'step_right',
            'put_red_tile', 'put_green_tile', 'remove_tile',
        ):
            self.tree.append([code_slice])
        elif value == 'while':
            self.tree.append([code_slice])
            self._condition = ConditionASTBuilder()
            self.next_slice = self._state_condition
        else:
            raise SyntaxError(
                'Unexpected "{}" at line: {}, pos: {}.'
                .format(code_slice.src_text, code_slice.line, code_slice.pos)
            )
        return True

    def _state_say_arg(self, code_slice: CodeSlice) -> bool:
        value: str = code_slice.value
        if value.startswith('"') and value.endswith('"'):
            code_slice.value = value[1:-1]
            self.tree[-1].append(code_slice)
            self.next_slice = self._state_start
        else:
            raise ValueError(
                'Argument for "say" at line: {}, pos: {} must be string in double quotes.'
                .format(code_slice.line, code_slice.pos)
            )
        return True

    def _state_move_to_arg1(self, code_slice: CodeSlice) -> bool:
        try:
            code_slice.value = int(code_slice.value)
            self.tree[-1].append(code_slice)
        except ValueError as err:
            raise ValueError(
                'Arguments for move_to at line: {}, pos: {} must be integer.'
                .format(code_slice.line, code_slice.pos)
            ) from err
        self.next_slice = self._state_move_to_arg2
        return True

    def _state_move_to_arg2(self, code_slice: CodeSlice) -> bool:
        try:
            code_slice.value = int(code_slice.value)
            self.tree[-1].append(code_slice)
        except ValueError as err:
            raise ValueError(
                'Arguments for move_to at line: {}, pos: {} must be integer.'
                .format(code_slice.line, code_slice.pos)
            ) from err
        self.next_slice = self._state_start
        return True

    def _state_condition(self, code_slice: CodeSlice) -> bool:
        if code_slice.value != '{':
            self._condition.next_slice(code_slice)
        else:
            self._condition.check()
            self.tree[-1].append(self._condition.tree)
            self._child_ast = CodeASTBuilder()
            self.next_slice = self._state_code_block
        return True

    def _state_else_prepare(self, code_slice: CodeSlice) -> bool:
        if code_slice.value == '{':
            self.next_slice = self._state_code_block
        else:
            raise SyntaxError(
                '"{}" expected, but found "{}" at line: {}, pos: {}.'
                .format('{', code_slice.src_text, code_slice.line, code_slice.pos)
            )
        return True

    def _state_code_block(self, code_slice: CodeSlice) -> bool:
        if self._child_ast.next_slice(code_slice):
            return True
        self._child_ast.check()
        self.tree[-1].append(self._child_ast.tree)
        self.next_slice = self._state_start
        return True

    def _state_finish(self, _) -> bool:
        raise RuntimeError('Attempting to use the machine in a finish state.')


def interpreter(source_code: str) -> list:
    """
    Строит дерево программы на основе исходного кода.

    :param source_code: Исходный код.
    :return: Абстрактное синтаксическое дерево программы.
    """
    slicer = CodeSlicer()
    slicer.slice_text(source_code)
    ast = CodeASTBuilder()
    for slice_ in slicer.slices:
        ast.next_slice(slice_)
    ast.check()
    return ast.tree
